#!/bin/sh
cd /home/espalier/Documents/generateurrepas/frontend/ 
git checkout develop
npm run build && notify-send -t 50 "BUILD Front" "Terminé."
cd /home/espalier/Documents/generateurrepas/backend/
mvn clean install -P prod && notify-send -t 50 "BUILD BACK" "Terminé"
cp -R /home/espalier/Documents/generateurrepas/frontend/dist /home/espalier/Documents/generateur_repas_prod
cp -R /home/espalier/Documents/generateurrepas/backend/target /home/espalier/Documents/generateur_repas_prod
cd /home/espalier/Documents/generateur_repas_prod
git add .
git commit -m "montée de version"
git push && notify-send -t 100 "JOB TERMINE" "Application prête et push sur gitlab"

